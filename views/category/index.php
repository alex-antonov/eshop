<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php foreach ($sales as $key => $sale) : ?>
                            <li data-target="#slider-carousel"
                                data-slide-to="<?= $key ?>" <?php if ($key === 0) echo 'class="active"' ?>></li>
                        <?php endforeach; ?>
                    </ol>

                    <div class="carousel-inner">
                        <?php foreach ($sales as $key => $sale) : ?>
                            <div class="item <?php if ($key === 0) echo 'active' ?>">
                                <div class="col-sm-6">
                                    <h1><?= $sale->name ?></h1>
                                    <h2><?= $sale->category->name ?></h2>
                                    <p><?= $sale->content ?></p>
                                    <a href="/card/add?id=<?= $sale->id ?>" data-id="9"
                                       class="btn btn-default add-to-cart"><i
                                                class="fa fa-shopping-cart"></i>Добавить в корзину</a>
                                </div>
                                <div class="col-sm-6">
                                    <?= Html::img("{$sale->getImage()->getUrl()}", ['alt' => $sale->name, 'class' => 'img-responsive']); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>

                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section><!--/slider-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Категории</h2>
                    <ul class="catalog category-products">
                        <?=
                        \app\components\MenuWidget::widget(['tpl' => 'menu'])
                        ?>
                    </ul>

                    <div class="shipping text-center"><!--shipping-->
                        <img src="images/home/shipping.jpg" alt=""/>
                    </div><!--/shipping-->
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <?php if (!empty($hits)) : ?>
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Популярные товары</h2>
                        <?php foreach ($hits as $hit) : ?>
                            <?php $img = $hit->getImage(); ?>
                            <div class="col-sm-4">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            <?= Html::img("{$img->getUrl()}", ['alt' => $hit->name]); ?>
                                            <h2>$<?= $hit->price ?></h2>
                                            <p>
                                                <a href="<?= \yii\helpers\Url::to(['product/view', 'id' => $hit->id]) ?>" class="product__name" ><?= $hit->name ?></a>
                                            </p>
                                            <a href="<?= \yii\helpers\Url::to(['card/add', 'id' => $hit->id]) ?>"
                                               data-id="<?= $hit->id ?>" class="btn btn-default add-to-cart"><i
                                                        class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </div>
                                        <?php if ($hit->new) : ?>
                                            <?= Html::img("@web/images/home/new.png", ['alt' => 'Новинка', 'class' => 'new']); ?>
                                        <?php endif; ?>
                                        <?php if ($hit->sale) : ?>
                                            <?= Html::img("@web/images/home/sale.png", ['alt' => 'Распродажа', 'class' => 'new']); ?>
                                        <?php endif; ?>
                                        <!--                                    <img src="images/home/new.png" class="new" alt="">-->
                                    </div>
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">
                                            <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div><!--features_items-->
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Категория</h2>
                    <ul class="catalog category-products">
                        <?=
                        \app\components\MenuWidget::widget(['tpl' => 'menu'])
                        ?>
                    </ul>

                    <div class="shipping text-center"><!--shipping-->
                        <img src="/images/home/shipping.jpg" alt=""/>
                    </div><!--/shipping-->

                </div>
            </div>
            <?php
            $mainImg = $product->getImage();
            $gallery = $product->getImages();
            ?>

            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product">
                            <?= Html::img("{$mainImg->getUrl()}", ['alt' => $product->name]); ?>
                            <h3><?= $product->name ?></h3>
                        </div>
                        <div id="similar-product" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $count = count($gallery);
                                $i = 0;
                                foreach ($gallery as $img) : ?>
                                    <?php if ($i % 3 == 0) : ?>
                                        <div class="item <?php if ($i == 0) echo "active" ?>">
                                    <?php endif; ?>
                                    <a href=""><?= Html::img("{$img->getUrl('84x85')}", ['alt' => $product->name]); ?></a>
                                    <?php $i++;
                                    if ($i % 3 == 0 || $i == $count) : ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                            <a class="left item-control" href="#similar-product" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right item-control" href="#similar-product" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                            <?php if ($product->new) : ?>
                                <?= Html::img("@web/images/product-details/new.jpg", ['alt' => 'Новинка', 'class' => 'newarrival']); ?>
                            <?php endif; ?>
                            <?php if ($product->sale) : ?>
                                <?= Html::img("@web/images/home/sale.png", ['alt' => 'Распродажа', 'class' => 'newarrival']); ?>
                            <?php endif; ?>
                            <h2><?= $product->name ?></h2>
                            <p>Web ID: <?= $product->id ?></p>
                            <img src="/images/product-details/rating.png" alt=""/>
                            <span>
									<span>US <?= $product->price ?></span>
									<label>Количество:</label>
									<input type="text" value="1" id="qty"/>
									<a data-id="<?= $product->id ?>" class="btn btn-fefault cart add-to-cart">
										<i class="fa fa-shopping-cart"></i>
										Add to cart
									</a>
								</span>
                            <p><b>Категория: </b><a
                                        href="<?= \yii\helpers\Url::to(['category/view', 'id' => $product->category->id]) ?>"><?= $product->category->name ?></a>
                            </p>
                            <?= $product->content ?>
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->


                <div class="recommended_items"><!--recommended_items-->
                    <h2 class="title text-center">Рекомендованные товары</h2>
                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php $i = 0;
                            $count = count($hits);
                            foreach ($hits as $hit) : ?>
                                <?php if ($i % 3 == 0) : ?>
                                    <div class="item <?php if ($i == 0) echo 'active' ?>">
                                <?php endif; ?>
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <?php $img = $hit->getImage(); ?>
                                                <?= Html::img("{$img->getUrl('')}", ['alt' => $hit->name]); ?>
                                                <h2><?= $hit->price ?></h2>
                                                <p>
                                                    <a href="<?= \yii\helpers\Url::to(['product/view', 'id' => $hit->id]) ?>" class="product__name"><?= $hit->name ?></a>
                                                </p>
                                                <button type="button" class="btn btn-default add-to-cart"><i
                                                            class="fa fa-shopping-cart"></i>Add to cart
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++;
                                if ($i % 3 == 0 || $i == $count) : ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div><!--/recommended_items-->

            </div>
        </div>
    </div>
</section>
<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\LtAppAsset;

AppAsset::register($this);
LtAppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home | E-Shopper</title>
        <link rel="shortcut icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
    <?php $this->beginBody() ?>
    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="tel:+2-95-01-88-821"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                <li><a href="mailto:info@webraccoon.pro"><i class="fa fa-envelope"></i> info@webraccoon.pro</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-vk"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->
        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="logo">
                            <a href="<?= \yii\helpers\Url::home() ?>"><?= Html::img('@web/images/home/logo.png', ['alt' => 'logo']) ?></a>
                        </div>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <div class="shop-menu">
                            <ul class="nav navbar-nav pull-right">
                                <li><a href="#" onclick="return getCard()"><i
                                                class="fa fa-shopping-cart"></i>Корзина</a>
                                </li>
                                <?php if (!Yii::$app->user->isGuest) : ?>
                                    <li><a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>"><i
                                                    class="fa fa-user"></i><?= Yii::$app->user->identity->username ?>
                                            (Выйти)</a></li>
                                <?php else : ?>
<!--                                    <li><a href="#"><i class="fa fa-star"></i> Избранное</a></li>-->
                                    <li><a href="<?= \yii\helpers\Url::to('/site/login') ?>"><i
                                                    class="fa fa-lock"></i>
                                            Вход</a></li>
                                    <li><a href="<?= \yii\helpers\Url::to('/site/registration') ?>"><i
                                                    class="fa fa-user""></i>
                                            Регистрация</a></li>
                                <?php endif; ?>
                            </ul>
                            <div class="search_box pull-right">
                                <form method="get" action="<? \yii\helpers\Url::to(['category/search']) ?>">
                                    <input type="text" placeholder="Search" name="q"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </header><!--/header-->

    <?= $content ?>

    <footer id="footer"><!--Footer-->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <p class="pull-left">Copyright © 2017.</p>
                    <p class="pull-right">Created by <span><a target="_blank"
                                                              href="https://webraccoon.pro">Webraccoon</a></span>
                    </p>
                </div>
            </div>
        </div>
    </footer><!--/Footer-->

    <?php
    \yii\bootstrap\Modal::begin([
        'header' => '<h2>Корзина</h2>',
        'id' => 'card',
        'size' => 'modal-lg',
        'footer' => '<button type="button" class="btn btn-default" data-dismiss= modal>Продолжить покупки</button>
        <a href="' . \yii\helpers\Url::to(['card/view']) . '" class="btn btn-success">Оформить заказ</a>
        <button type="button" class="btn btn-danger" onclick="clearCard()">Очистить корзину</button>'

    ]);
    \yii\bootstrap\Modal::end();
    ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>